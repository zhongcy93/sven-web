//
// Created by 钟乘永 on 2018/8/18.
//

#include <cstdlib>
#include <ctime>
#include <memory>
#include <string>
#include <algorithm>
#include "base/logger.h"

sven::LOG_LEVEL sven::ConvertStringToLogLevel(const std::string &str) {
    std::string tmp;
    tmp.resize(str.size());
    transform(str.begin(), str.end(), tmp.begin(), tolower);
    if ("debug" == tmp) {
        return sven::DEBUG;
    } else if ("info" == tmp) {
        return sven::INFO;
    } else if ("warn" == tmp) {
        return sven::WARN;
    } else if ("error" == tmp) {
        return sven::ERROR;
    } else if ("fatal" == tmp) {
        return sven::FATAL;
    }
    throw std::invalid_argument("Log level string errror, should be one of debug/info/warn/error/fatal.");
}

void sven::InitLogger(const std::string &debug_file, const std::string &info_file, const std::string &warn_file,
                      const std::string &error_file, const std::string &fatal_file, sven::LOG_LEVEL log_level) {
    SetDebugFile(debug_file);
    SetInfoFile(info_file);
    SetWarnFile(warn_file);
    SetErrorFile(error_file);
    SetFatalFile(fatal_file);
    SetLogLevel(log_level);
}

std::ofstream sven::Logger::null_stream_(0);

std::ofstream sven::Logger::debug_file_;
std::ofstream sven::Logger::info_file_;
std::ofstream sven::Logger::warn_file_;
std::ofstream sven::Logger::error_file_;
std::ofstream sven::Logger::fatal_file_;
sven::LOG_LEVEL sven::Logger::log_level_ = DEBUG;

std::ostream &sven::Logger::GetStream(sven::LOG_LEVEL level) {
    if (sven::DEBUG == level) {
        return debug_file_.is_open() ? debug_file_ : std::cout;
    } else if (sven::INFO == level) {
        return info_file_.is_open() ? info_file_ : std::cout;
    } else if (sven::WARN == level) {
        return warn_file_.is_open() ? warn_file_ : std::cout;
    } else if (sven::ERROR == level) {
        return error_file_.is_open() ? error_file_ : std::cerr;
    } else if (sven::FATAL == level) {
        return fatal_file_.is_open() ? fatal_file_ : std::cerr;
    }
    throw std::invalid_argument("Log level error.");
}

std::ostream &sven::Logger::Log(sven::LOG_LEVEL log_level, const int line, const std::string &function) {
    //当日志级别低于当前级别时，不记录日志。
    if (log_level < Logger::log_level_) {
        return sven::Logger::null_stream_;
    }
    time_t t = time(0);
    char tmpBuf[156];
    strftime(tmpBuf, 156, "%Y%m%d%H%M%S", localtime(&t));  // 格式化日期和时间

    std::string level_string = "";
    switch (log_level) {
        case sven::DEBUG:
            level_string = "debug";
            break;
        case sven::INFO:
            level_string = "info";
            break;
        case sven::WARN:
            level_string = "warn";
            break;
        case sven::ERROR:
            level_string = "error";
            break;
        case sven::FATAL:
            level_string = "fatal";
            break;
    }

    return GetStream(log_level) << "[" << tmpBuf << "]"
                                << "--"
                                << "[" << level_string << "]"
                                << "--"
                                << "function (" << function << ")"
                                << "line " << line << ":";
}

sven::Logger::~Logger() {
    GetStream(log_level_) << std::endl << std::flush;
    if (sven::FATAL == log_level_) {
        debug_file_.close();
        info_file_.close();
        warn_file_.close();
        error_file_.close();
        fatal_file_.close();
    }
}