//
// Created by 钟乘永 on 2018/8/21.
//

#ifndef SVEN_WEB_UTIL_H
#define SVEN_WEB_UTIL_H

#include <string>
#include <vector>

namespace sven {
    void EraseSpace(std::string &s);

    void EraseAllSpace(std::string &s);

    inline void LTrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) { return !std::isspace(ch); }));
    };

    inline void RTrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) { return !std::isspace(ch); }).base(), s.end());
    };

    inline void Trim(std::string &s) {
        LTrim(s);
        RTrim(s);
    };

    void SplitString(std::string &s, const std::string &p, std::vector<std::string> &result);


}


#endif //SVEN_WEB_UTIL_H
