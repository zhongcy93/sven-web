//
// Created by 钟乘永 on 2018/8/18.
//

#ifndef SVEN_WEB_LOGGER_H
#define SVEN_WEB_LOGGER_H

#include <fstream>
#include <iostream>

//Note: 枚举打印的值为index。
namespace sven {
    enum LOG_LEVEL {
        DEBUG,
        INFO,
        WARN,
        ERROR,
        FATAL
    };

    void test();

    LOG_LEVEL ConvertStringToLogLevel(const std::string &str);

    void InitLogger(const std::string &debug_file, const std::string &info_file,
                    const std::string &warn_file, const std::string &error_file,
                    const std::string &fatal_file, LOG_LEVEL log_level);

    void SetLogLevel(LOG_LEVEL &log_level);

    void SetLevelFile(std::ofstream &, const std::string &);

    void SetDebugFile(const std::string &);

    void SetInfoFile(const std::string &);

    void SetWarnFile(const std::string &);

    void SetErrorFile(const std::string &);

    void SetFatalFile(const std::string &);

    class Logger {
    public:
        static Logger &GetLoggerInstance() {
            static Logger loggerInstance;
            return loggerInstance;
        }

        std::ostream &GetStream(LOG_LEVEL level);

        std::ostream &Log(LOG_LEVEL log_level, const int line, const std::string &function);

        /*
         * 友元函数笔记：友元函数不是任何类的成员函数，但是可以访问类的成员变量。
         * 实现时，没有Class::
         */
        friend void SetLogLevel(LOG_LEVEL &log_level) {
            log_level_ = log_level;
        };

        friend void SetLevelFile(std::ofstream &stream, const std::string &file) {
            if (stream.is_open()) {
                stream.close();
            }
            stream.open(file.c_str(), std::ios::in | std::ios::app);
            if (!stream.is_open()) {
                std::cout << "Can not open \"" << file << "\"" << std::endl;
            }
        };

        friend void SetDebugFile(const std::string &file) {
            sven::SetLevelFile(debug_file_, file);
        };

        friend void SetInfoFile(const std::string &file) {
            sven::SetLevelFile(info_file_, file);
        };

        friend void SetWarnFile(const std::string &file) {
            sven::SetLevelFile(warn_file_, file);
        };

        friend void SetErrorFile(const std::string &file) {
            sven::SetLevelFile(warn_file_, file);
        };

        friend void SetFatalFile(const std::string &file) {
            sven::SetLevelFile(fatal_file_, file);
        };

        friend void InitLogger(const std::string &debug_file, const std::string &info_file,
                               const std::string &warn_file, const std::string &error_file,
                               const std::string &fatal_file, LOG_LEVEL log_level);

        ~Logger();

    private:
        static LOG_LEVEL log_level_;

        static std::ofstream null_stream_;
        static std::ofstream debug_file_;
        static std::ofstream info_file_;
        static std::ofstream warn_file_;
        static std::ofstream error_file_;
        static std::ofstream fatal_file_;

        //TODO 确定是否使用正确
        Logger() = default;


    };

    #define LOG(level) Logger::GetLoggerInstance().Log(level, __LINE__, __FUNCTION__)
}
#endif //SVEN_WEB_LOGGER_H
