//
// Created by 钟乘永 on 2018/8/20.
//

#include "base/configer.h"
#include "base/util.h"

#include <algorithm>
#include <vector>
#include <fstream>

std::string sven::Configer::config_file_ = "";

sven::Configer::Configer() {
    InitDefaultValue();
}

void sven::Configer::SetConfigFile(const std::string &file) {
    config_file_ = file;
}

sven::Configer::~Configer() {

}

void sven::Configer::InitDefaultValue() {
    config_map_["listen"] = "80";
    config_map_["processpoll"] = "8";
    config_map_["docs"] = "";
    config_map_["hostname"] = "";
    config_map_["loglevel"] = "Info";
    config_map_["logpath"] = "";
    config_map_["debugfile"] = "";
    config_map_["infofile"] = "";
    config_map_["warnfile"] = "";
    config_map_["errorfile"] = "";
    config_map_["fatalfile"] = "";
}

bool sven::Configer::ParseLine(std::string &s, std::string &key, std::string &value) {
    sven::Trim(s);
    if (s.empty()) {
        return true;
    }else if (s[0] == '#'){
        return true;
    }
    std::vector<std::string> argv;
    sven::SplitString(s, "=", argv);
    if (argv.size() > 2 || argv.size() == 0) {
        return false;
    }

    // Note：调用resize函数将字符串的长度重新调整为参数n
    key.resize(argv[0].size());
    transform(argv[0].begin(), argv[0].end(), key.begin(), tolower);

    if (argv.size() == 2){
        value = argv[1];
    }

    sven::Trim(key);
    sven::Trim(value);
    return true;
}

bool sven::Configer::LoadConfig() {
    if (sven::Configer::config_file_.empty()){
        throw std::invalid_argument("Set the config file first");
    }
    std::ifstream infile(config_file_);
    std::string line;
    std::string key, value;
    while (getline(infile, line)) {
        if (ParseLine(line, key, value)) {
            auto p = config_map_.find(key);
            if (p == config_map_.end()) {
                return false; // Not in the listed keys
            }
            if (!key.empty()){
                config_map_[key] = value;
            }
        }else{
            return false;
        }
    }
    return true;
}

std::string sven::Configer::GetConfigValue(const std::string &key) {
    if (config_map_.find(key) != config_map_.end()) {
        return config_map_[key];
    }
    return "";
}
