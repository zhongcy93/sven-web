//
// Created by 钟乘永 on 2018/8/21.
//

#include "base/util.h"

/*
 * Note:使用string的find_first/last_not_of()函数查找首尾的空格，并去除。
 * 传入find函数的参数如果是单个字符串，使用char。
 */
void sven::EraseSpace(std::string &s) {
    if (!s.empty()) {
        s.erase(0, s.find_first_not_of(' '));
        s.erase(s.find_last_not_of(' ') + 1);
    }
}

void sven::EraseAllSpace(std::string &s) {
    if (!s.empty()) {
        std::size_t index = 0;
        while ((index = s.find(' ', index)) != std::string::npos) {
            s.erase(index, 1);
        }
    }
}


void sven::SplitString(std::string &s, const std::string &p, std::vector<std::string> &result) {
    std::string::size_type begin, end;
    begin = 0;
    end = s.find(p, begin);

    while (std::string::npos != end) {
        result.push_back(s.substr(begin, end - begin));
        begin = end + p.size();
        end = s.find(p, begin);
    }
    result.push_back(s.substr(begin, end - begin));
}