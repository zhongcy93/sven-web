//
// Created by 钟乘永 on 2018/8/20.
//

#ifndef SVEN_WEB_CONFIGER_H
#define SVEN_WEB_CONFIGER_H

#include <string>
#include <map>

void SetConfigFile(const std::string &file);

bool LoadConfig();

std::string GetConfigValue(const std::string &key);

namespace sven {
    class Configer {
    private:
        static std::string config_file_;

        std::map<std::string, std::string> config_map_;

        void InitDefaultValue();

        //解析一行，得到key&value
        bool ParseLine(std::string &s, std::string &key, std::string &value);

        Configer();

        Configer(const Configer &c) {}  //设置为private
    public:
        static Configer &GetConfigerInstance() {
            static Configer instance;
            return instance;
        }

        void SetConfigFile(const std::string &file);

        bool LoadConfig();

        std::string GetConfigValue(const std::string &key);

        ~Configer();
    };
}


#endif //SVEN_WEB_CONFIGER_H
