//
// Created by 钟乘永 on 2018/2/25.
//

#include "google_test_main.h"
#include "base/logger.h"
#include "base/configer.h"
#include "base/util.h"

#include <ctime>
#include <iostream>
#include <unistd.h>
#include <fstream>

using namespace sven;

TEST(TEST1, CASE1) {
    EXPECT_EQ(1, 1);
}

TEST(LOGGER, ERROR_STRING) {
    LOG_LEVEL l = ConvertStringToLogLevel("info");
    std::cout << "Convert level:" << l << std::endl;
}

TEST(LOGGER, STD_OUT) {
    InitLogger("~/debug.log", "~/info.log", "~/warn.log", "~/error.log", "~/fatal.log", LOG_LEVEL::DEBUG);
    for (LOG_LEVEL l = LOG_LEVEL::DEBUG; l <= LOG_LEVEL::FATAL; l = LOG_LEVEL(l + 1)) {
        SetLogLevel(l);
        LOG(LOG_LEVEL::DEBUG) << std::endl;
    }
}

TEST(CONFIGER, INSTANCE) {
    Configer &c1 = Configer::GetConfigerInstance();
    Configer &c2 = Configer::GetConfigerInstance();
    EXPECT_EQ(&c1, &c2);
}

TEST(UTIL, UTIL_ERASE_Test){
    std::string s = " Hello, World.  ";
    EraseSpace(s);
    EXPECT_EQ(s, "Hello, World.");
    s = " Hello, World.  ";
    EraseAllSpace(s);
    EXPECT_EQ(s, "Hello,World.");
}

TEST(UTIL, UTIL_SPLIT_Test){
    std::string s = "hello, world, I,am,zhongcy ";
    std::vector<std::string> v;
    SplitString(s, ",", v);
    EXPECT_EQ("hello", v[0]);
}

TEST(UT, FILEREADER){
    std::string file_path = "/Users/zhongchengyong/opt/projects/sven-web/test/config.txt";
    // Note：一定要使用引用，否则将调用拷贝构造函数
    Configer &configer = Configer::GetConfigerInstance();
    configer.SetConfigFile(file_path);
    configer.LoadConfig();
    EXPECT_EQ(configer.GetConfigValue("loglevel"), "debug");
}